c------------------------------------------------------------------------
      SUBROUTINE UPDOWN (N,XI,FI,X,F,DF)
      implicit double precision(a-h,o-z)
C
C Subroutine performing the Lagrange interpolation with the
C upward and downward correction method.  F: interpolated
C value.  DF: error estimated.
C Copyright (c) Tao Pang 1997.
C
      PARAMETER (NMAX=27) 
      DIMENSION XI(N),FI(N),DP(NMAX,NMAX),DM(NMAX,NMAX)
C
      IF (N.GT.NMAX) STOP 'Dimension too large'
      DX = ABS(XI(N)-XI(1))
      DO     100 I = 1, N
        DP(I,I) = FI(I)
        DM(I,I) = FI(I)
        DXT = ABS(X-XI(I))
        IF (DXT.LT.DX) THEN
           I0 = I
           DX = DXT
        ELSE
        END IF
  100 CONTINUE
      J0 = I0
C
C Evaluate correction matrices
C
      DO     200 I = 1, N-1
        DO   150 J = 1, N-I
          K = J+I
          DT =(DP(J,K-1)-DM(J+1,K))/(XI(K)-XI(J))
          DP(J,K) = DT*(XI(K)-X)
          DM(J,K) = DT*(XI(J)-X)
  150   CONTINUE
  200 CONTINUE
C
C Update the approximation
C
      F = FI(I0)
      IT = 0
      IF(X.LT.XI(I0)) IT = 1
      DO     300 I = 1, N-1
        IF ((IT.EQ.1).OR.(J0.EQ.N)) THEN
          I0 = I0 - 1
          DF = DP(I0,J0)
          F  = F + DF
          IT = 0
          IF (J0.EQ.N) IT = 1
        ELSE IF ((IT.EQ.0).OR.(I0.EQ.1)) THEN
          J0 = J0 + 1
          DF = DM(I0,J0)
          F = F + DF
          IT = 1
          IF (I0.EQ.1) IT = 0
        END IF
  300 CONTINUE
      DF = ABS(DF)
      RETURN
      END
! Interpolacja motodą funkcji sklejanych
c------------------------------------------------------------------------
      SUBROUTINE ZSPL3(N,T,Y,H,B,U,V,Z)
      implicit double precision(a-h,o-z)
      DIMENSION  T(N),Y(N),H(N),B(N),U(N),V(N),Z(N)       
      DO 2 I = 1,N-1
        H(I) = T(I+1) - T(I)
        B(I) = (Y(I+1) -Y(I))/H(I)    
   2  CONTINUE    
      U(2) = 2.0*(H(1) + H(2))
      V(2) = 6.0*(B(2) - B(1))
      DO 3 I = 3,N-1
        U(I) = 2.0*(H(I) + H(I-1)) - H(I-1)**2/U(I-1)     
        V(I) = 6.0*(B(I) - B(I-1)) - H(I-1)*V(I-1)/U(I-1) 
   3  CONTINUE    
      Z(N) = 0.0  
      DO 4 I = N-1,2,-1     
        Z(I) = (V(I) - H(I)*Z(I+1))/U(I)
   4  CONTINUE    
      Z(1) = 0.0
      RETURN
      END 
c------------------------------------------------------------------------
      FUNCTION SPL3(N,T,Y,Z,X)
      implicit double precision(a-h,o-z)
      DIMENSION  T(N),Y(N),Z(N)       
      DO 2 I = N-1,1,-1     
        DIFF = X - T(I)     
        IF(DIFF .GE. 0.0)  GO TO 3    
   2  CONTINUE    
      I = 1 
   3  H = T(I+1) - T(I)     
      B = (Y(I+1) - Y(I))/H - H*(Z(I+1) + 2.0*Z(I))/6.0 
      P = 0.5*Z(I) + DIFF*(Z(I+1) - Z(I))/(6.0*H) 
      P = B + DIFF*P
      SPL3 = Y(I) + DIFF*P  
      RETURN      
      END 
c------------------------------------------------------------------------
      complex*16 function funkcjakulista(l,m,costh,phi)
      implicit double precision (a-h,o-z)
      complex*16 CI
      PI=3.141592653589793116d0
      CI=DCMPLX(0.D0,1.D0)
      sinth=DSQRT(1.d0-costh**2)
      if (l.eq.0) then
        funkcjakulista=1.d0/(2.d0*DSQRT(Pi))
      else if (l.eq.1) then
        if(m.eq.-1) then
        funkcjakulista=DSQRT(3.d0/(2.d0*Pi))*(CDEXP(-CI*phi)
     &  *sinth)/2.d0
        else if(m.eq.0) then
        funkcjakulista=(DSQRT(3.d0/Pi)*costh)/2.d0
        else if (m.eq.1) then
        funkcjakulista=-DSQRT(3.d0/(2.d0*Pi))*(CDEXP(CI*phi)
     &  *sinth)/2.d0
        endif
      else if (l.eq.2) then
        if (m.eq.-2) then
        funkcjakulista=(CDEXP(-2.d0*CI*phi)*DSQRT(15.d0/(2.d0*Pi))
     &  *sinth**2)/4.d0
        else if (m.eq.-1) then
        funkcjakulista=(CDEXP(-CI*phi)*DSQRT(15.d0/(2.d0*Pi))*
     &  costh*sinth)/2.d0
        else if (m.eq.0) then
        funkcjakulista=(DSQRT(5.d0/Pi)*(-1.d0+3.d0*costh**2))/
     &  4.d0
        else if (m.eq.1) then
        funkcjakulista=-(CDEXP(CI*phi)*DSQRT(15.d0/(2.d0*Pi))*
     &  costh*sinth)/2.d0
        else if (m.eq.2) then
        funkcjakulista=(CDEXP(2.d0*CI*phi)*DSQRT(15.d0/(2.d0*Pi))
     &  *sinth**2)/4.d0
        endif
      else if (l.eq.3) then
        if (m.eq.-3) then
        funkcjakulista=(CDEXP(-3.d0*CI*phi)*DSQRT(35.d0/Pi)*sinth**3)
     &  /8.d0
        else if (m.eq.-2) then
        funkcjakulista=(CDEXP(-2.d0*CI*phi)*DSQRT(105.d0/(2.d0*PI))*
     &  costh*sinth**2)/4.d0
        else if (m.eq.-1) then
        funkcjakulista=CDEXP(-CI*phi)*DSQRT(21.d0/Pi)*
     &  (-1.d0+5.d0*costh**2)*sinth/8.d0
        else if (m.eq.0) then
        funkcjakulista=(DSQRT(7.d0/Pi)*(-3.d0*costh+5.d0*
     &  costh**3))/4.d0
        else if (m.eq.1) then
        funkcjakulista=-(CDEXP(CI*phi)*DSQRT(21.d0/Pi)*
     &  (-1.d0+5.d0*costh**2)*sinth)/8.d0
        else if (m.eq.2) then
        funkcjakulista=(CDEXP(2.d0*CI*phi)*DSQRT(105.d0/(2.d0*Pi))
     &  *costh*sinth**2)/4.d0
        else if (m.eq.3) then
        funkcjakulista=(-CDEXP(3.d0*CI*phi)*DSQRT(35.d0/Pi)*sinth**3)
     &  /8.d0
        endif
      else if (l.eq.4) then
        if (m.eq.-4) then
        funkcjakulista=3.d0/16.d0*CDEXP(-4.d0*CI*phi)*
     &  DSQRT(35.d0/(2.d0*PI))*sinth**4
        else if (m.eq.-3) then
        funkcjakulista=3.d0/8.d0*CDEXP(-3.d0*CI*phi)*DSQRT(35.d0/Pi)*
     &  costh*sinth**3
        else if (m.eq.-2) then
        funkcjakulista=3.d0/8.d0*CDEXP(-2.d0*CI*phi)*
     &  DSQRT(5.d0/(2.d0*Pi))*(-1.d0+7.d0*costh**2)*sinth**2
        else if (m.eq.-1) then
        funkcjakulista=3.d0/8.d0*CDEXP(-CI*phi)*DSQRT(5.d0/Pi)*costh*
     &  (-3.d0+7.d0*costh**2)*sinth
        else if (m.eq.0) then
        funkcjakulista=(3.d0*(3.d0-30.d0*costh**2+35.d0*
     &  costh**4))/(16.d0*DSQRT(Pi))
        else if (m.eq.1) then
        funkcjakulista=(-3.d0*CDEXP(CI*phi)*DSQRT(5.d0/Pi)*costh*
     &  (-3.d0+7.d0*costh**2)*sinth)/8.d0
        else if (m.eq.2) then
        funkcjakulista=(3.d0*CDEXP(2.d0*CI*phi)*DSQRT(5.d0/(2.d0*Pi))*
     &  (-1.d0+7.d0*costh**2)*sinth**2)/8.d0
        else if (m.eq.3) then
        funkcjakulista=-3.d0/8.d0*CDEXP(3.d0*CI*phi)*DSQRT(35.d0/Pi)*
     &  costh*sinth**3
        else if (m.eq.4) then
        funkcjakulista=3.d0/16.d0*CDEXP(4.d0*CI*phi)*
     &  DSQRT(35.d0/(2.d0*Pi))*sinth**4
        endif
      else if (l.eq.5) then
        if(m.eq.-5) then
        funkcjakulista=3.d0/32.d0*CDEXP(-5.d0*CI*phi)*DSQRT(77.d0/Pi)*
     &  sinth**5
        else if(m.eq.-4) then
        funkcjakulista=3.d0/16.d0*CDEXP(-4.d0*CI*phi)*
     &  DSQRT(385.d0/(2.d0*Pi))*costh*sinth**4
        else if(m.eq.-3) then
        funkcjakulista=1.d0/32.d0*CDEXP(-3.d0*CI*phi)*DSQRT(385.d0/Pi)*
     &  (-1.d0+9.d0*costh**2)*sinth**3
        else if(m.eq.-2) then
        funkcjakulista=1.d0/8.d0*CDEXP(-2.d0*CI*phi)*
     &  DSQRT(1155.d0/(2.d0*Pi))*costh*(-1.d0+3.d0*costh**2)*sinth**2
        else if(m.eq.-1) then
        funkcjakulista=1.d0/16.d0*CDEXP(-CI*Pi)*DSQRT(165.d0/(2.d0*Pi))
     &  *(1.d0-14.d0*costh**2+21.d0*costh**4)*sinth
        else if(m.eq.0) then
        funkcjakulista=1.d0/16.d0*DSQRT(11.d0/Pi)*
     &  (15.d0*costh-70.d0*costh**3+63.d0*costh**5)
        else if(m.eq.1) then
        funkcjakulista=-1.d0/16.d0*CDEXP(CI*Pi)*
     &  DSQRT(165.d0/(2.d0*Pi))*(1.d0-14.d0*costh**2+21.d0*costh**4)*
     &  sinth
        else if(m.eq.2) then
        funkcjakulista=1.d0/8.d0*CDEXP(2.d0*CI*Pi)*
     &  DSQRT(1155.d0/(2.d0*Pi))*costh*(-1.d0+3.d0*costh**2)*sinth**2
        else if(m.eq.3) then
        funkcjakulista=-1.d0/32.d0*CDEXP(3.d0*CI*phi)*DSQRT(385.d0/Pi)*
     &  (-1.d0+9.d0*costh**2)*sinth**3
        else if(m.eq.4) then
        funkcjakulista=3.d0/16.d0*CDEXP(4.d0*CI*phi)*
     &  DSQRT(385.d0/(2.d0*Pi))*costh*sinth**4
        else if(m.eq.5) then
        funkcjakulista=-3.d0/32.d0*CDEXP(5.d0*CI*phi)*
     &  DSQRT(77.d0/Pi)*sinth**5
        endif
      else
      print*,"l-value beyond the scope..."
      endif
      end 
c------------------------------------------------------------------------
! pochodne wielomianów Legendre'a
      double precision function LegendrePrim(n,x)
      implicit double precision (a-h,o-z)
      if (n.eq.0) then
      LegendrePrim=0.d0
      else if (n.eq.1) then
      LegendrePrim=1.d0
      else if (n.eq.2) then
      LegendrePrim=3.d0*x
      else if (n.eq.3) then
      LegendrePrim=0.5d0*(15.d0*x**2-3.d0)
      else if (n.eq.4) then
      LegendrePrim=1.d0/8.d0*(140.d0*x**3-60.d0*x)
      else if (n.eq.5) then
      LegendrePrim=(15.d0-210.d0*x**2+315.d0*x**4)/8.d0
      endif
      end
c------------------------------------------------------------------------
! funkcje Legendre'a drugiego rodzaju
      double precision function LegendreQ(n,x)
      integer n
      double precision x
      if (n.eq.0) then
      LegendreQ=-0.5d0*dlog(x-1.d0)+0.5d0*dlog(1.d0+x)
      else if (n.eq.1) then
      LegendreQ=-1.d0+x*(-0.5d0*dlog(x-1.d0)+0.5d0*dlog(1.d0+x))
      else if (n.eq.2) then
      LegendreQ=-3.d0*x/2.d0+0.5d0*(-1.d0+3.d0*x**2)*
     &(-0.5d0*dlog(x-1.d0)+0.5d0*dlog(1.d0+x))
      else if (n.eq.3) then
      LegendreQ=2.d0/3.d0-5.d0/2.d0*x**2-0.5d0*x*(3.d0-5.d0*x**2)*
     &(-0.5d0*dlog(x-1.d0)+0.5d0*dlog(1.d0+x))
      else if (n.eq.4) then
      LegendreQ=55.d0/24.d0*x-35.d0/8.d0*x**3+1.d0/8.d0*
     &(3.d0-30.d0*x**2+35.d0*x**4)*
     &(-0.5d0*dlog(x-1.d0)+0.5d0*dlog(1.d0+x))
      else if (n.eq.5) then
      LegendreQ=-8.d0/15.d0+49.d0/8.d0*x**2-63.d0/8.d0*x**4+
     &1.d0/8.d0*x*(15.d0-70.d0*x**2+63.d0*x**4)*
     &(-0.5d0*dlog(x-1.d0)+0.5d0*dlog(1.d0+x))
      else if (n.eq.6) then
      LegendreQ=-231.d0/80.d0*x+119.d0/8.d0*x**3-231.d0/16.d0*x**5+
     &1.d0/16.d0*(-5.d0+105.d0*x**2-315.d0*x**4+231.d0*x**6)*
     &(-0.5d0*dlog(x-1.d0)+0.5d0*dlog(1.d0+x))
      endif
      end
