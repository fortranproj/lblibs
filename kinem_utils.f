      real*8 function xLbd(x1,x2,x3)
      implicit none
      real*8 x1,x2,x3
      xLbd=x1**2+x2**2+x3**2-2.d0*(x1*x2+x1*x3+x2*x3)
      end
